<?php

if(isset($_POST['search'])){
    $searchText=$_POST['search'];
    if(strlen($searchText)>0){
        if($con->connect_error){
            echo 'Connection Faild: '.$con->connect_error;
        }else{
            $sql = "select *, 'events' from events where title like '%$searchText%' or date like '%$searchText%' or body like '%$searchText%';";
            // $sql .= "select *,'message' from message where name like '%$searchText%' or message like '%$searchText%'; ";
            // $sql .= "select *, 'about_us' from about_us where details like '%$searchText%' or history like '%$searchText%' or profile like '%$searchText%' or management like '%$searchText%'; ";
            $sql .= "select *, 'career' from career where content like '%$searchText%'; ";
            // $sql .= "select *, 'categories' from categories where name like '%$searchText%'; ";
            $sql .= "select *, 'download' from download where name like '%$searchText%' or file like '%$searchText%'; ";
            $sql .= "select *, 'facilities' from facilities where name like '%$searchText%' or content like '%$searchText%';";
            $sql .= "select *, 'news' from news where title like '%$searchText%' or content like '%$searchText%' or date like '%$searchText%';";
            $sql .= "select *, 'programs' from programs where title like '%$searchText%' or content like '%$searchText%'";
            if(mysqli_multi_query($con,$sql))
            {
                do
                {
                    if ($result=mysqli_store_result($con)) {
                        $counter =1;
                        while ($row=mysqli_fetch_row($result))
                        {
                            // printf("%s\t,%s\t,%s\t,%s\t,%s\t \n",$row[0], $row[1], $row[2], $row[3], $row[4]);
                            // echo end($row);
                            if(end($row)=='events'){
                                $id = url($row[0]);
                                $event_title = $row[2];
                                $event_body = $row[3];
                                $date = $row[1];
                                $yrdata = strtotime($date);

                                $res ="<div class='row event-list py-2'>
                                          <div class='col-lg-4'>
                                          <ul class='event-day text-center border'>
                                          <li class='bg-event py-2' id='nepMonth".$counter."'>".date('F', $yrdata)."</li>
                                          <li class='nepDay bg-light py-2' id='nepDay".$counter."'>".date('jS', $yrdata)."</li>
                                          </ul>
                                          
                                          </div>
                                          <div class='col-lg-8'>
                                          <b>$event_title</b>
                                          <p>$event_body</p>
                                          <hr>
                                          </div>
                                        </div><script type='text/javascript'>
                                            var date='".$date."';date=ad2bs(date.split('-').join('/'));
                                            document.getElementById('nepMonth".$counter."').innerHTML=date.ne.strMonth;
                                            document.getElementById('nepDay".$counter."').innerHTML=date.ne.day;
                                        </script>";

                                echo $res;
                                ++$counter;
                            }
                            // if(end($row)=='message'){

                            // }
                            // if(end($row)=='about_us'){

                            // }
                            if(end($row)=='career'){
                                $content = $row[1];
                                echo $content;
                            }
                            // if(end($row)=='categories'){

                            // }
                            if(end($row)=='download'){
                                $content = $row[3];
                                echo $content;
                            }
                            if(end($row)=='facilities'){
                                $id = $row[0];
                                $title = $row[1];
                                $image = $row[2];
                                $content = $row[3];
                                $content = substr($content,0, 80);

                                $res ="<div class='row news-list py-2'>
                                            <div class='col-lg-4'>
                                            <div class='news-img'>
                                            <img src='$image' class='img-fluid' alt=''>
                                            </div> 
                                            </div>
                                            <div class='col-lg-8'>
                                            <b>".ucwords($title)."</b>
                                            <p> $content ...</p>
                                            <a href='".url_for('/pages?url=facilities&id='.$id.'')."' class='btn btn-sm btn-outline-primary'>Read More</a>
                                            </div>
                                            </div>";

                                echo $res;
                            }
                            if(end($row)=='news'){

                                $image = $row[3];
                                $title = $row[1];
                                $content = $row[4];
                                $content = substr($content,0, 80);
                                $id = $row[0];
                                $res ="<div class='row news-list py-2'>
                                            <div class='col-lg-4'>
                                            <div class='news-img'>
                                            <img src='$image' class='img-fluid' alt=''>
                                            </div> 
                                            </div>
                                            <div class='col-lg-8'>
                                            <b>".ucwords($title)."</b>
                                            <p> $content ...</p>
                                            <a href='".url_for('/pages?url=news&id='.$id.'')."' class='btn btn-sm btn-outline-primary'>Read More</a>
                                            </div>
                                            </div>";

                                echo $res;

                            }
                            if(end($row)=='programs'){

                                viewProgram($con,$row[0]);
                                // echo 'i am programs';
                                // printf("\n");
                            }


                            // echo count($row);
                        }

                        mysqli_free_result($result);
                    }
                }
                while (mysqli_next_result($con));
            }
        }
    }
    else{
        echo 'No any input detected in search field!!';
    }


}else{
    header('Location:/public');
}
?>