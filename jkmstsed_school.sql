-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 27, 2018 at 03:36 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jkmstsed_school`
--

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE `programs` (
  `id` int(11) NOT NULL,
  `file_path` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`id`, `file_path`, `title`, `content`) VALUES
(1, '', 'Nursury-UKG', '<p>à¤¶à¥ˆà¤•à¥à¤·à¤¿à¤• à¤ªà¤¾à¤ à¥à¤¯à¤•à¥à¤°à¤®à¤•à¤¾ à¤•à¥à¤·à¥‡à¤¤à¥à¤°à¤®à¤¾ à¤•à¤•à¥à¤·à¤¾ à¥§ à¤¦à¥‡à¤–à¤¿ à¥® à¤¸à¤®à¥à¤® à¤…à¤‚à¤—à¥à¤°à¥‡à¤œà¥€ à¤° à¤¨à¥‡à¤ªà¤¾à¤²à¥€ à¤¦à¥à¤µà¥ˆ à¤®à¤¾à¤§à¥à¤¯à¤®à¤¬à¤¾à¤Ÿ à¤ªà¤ à¤¨à¤ªà¤¾à¤ à¤¨ à¤¸à¥à¤°à¥ à¤—à¤°à¥‡à¤•à¥‹ à¤› à¤­à¤¨à¥‡ à¤à¤¸.à¤ˆ.à¤ˆ. à¤ªà¤°à¥€à¤•à¥à¤·à¤¾à¤®à¤¾ à¤§à¥‡à¤°à¥ˆ à¤ªà¤Ÿà¤• à¤œà¤¿à¤²à¥à¤²à¤¾à¤®à¤¾ à¤¸à¤°à¥à¤µà¥‹à¤¤à¥à¤•à¥ƒà¤·à¥à¤Ÿ à¤¸à¥à¤¥à¤¾à¤¨ à¤¸à¤®à¥‡à¤¤ à¤ªà¥à¤°à¤¾à¤ªà¥à¤¤ à¤—à¤°à¤¿à¤¸à¤•à¥‡à¤•à¥‹ à¤› à¥¤</p>'),
(2, '../img/posts/5b96556fc9a0dderp.jpeg', 'Class 1-5', '<p>à¤¸à¥à¤µà¤°à¥‹à¤œà¤—à¤¾à¤°à¤®à¥‚à¤²à¤•, à¤œà¥€à¤µà¤¨à¥‹à¤ªà¤¯à¥‹à¤—à¥€ à¤° à¤µà¤¿à¤¶à¥à¤µ à¤µà¤œà¤¾à¤°à¤®à¤¾ à¤ªà¥à¤°à¤¤à¤¿à¤·à¥à¤ªà¤°à¥à¤§à¤¾ à¤—à¤°à¥à¤¨ à¤¸à¤•à¥à¤¨à¥‡ à¤œà¤¨à¤¶à¤•à¥à¤¤à¤¿ à¤‰à¤¤à¥à¤ªà¤¾à¤¦à¤¨ à¤—à¤°à¥à¤¨à¥‡ à¤‰à¤¦à¥à¤¦à¥‡à¤¶à¥à¤¯à¤•à¤¾ à¤¸à¤¾à¤¥ à¤¸à¥à¤¥à¤¾à¤ªà¤¨à¤¾ à¤­à¤à¤•à¥‹ à¤¯à¤¸ à¤µà¤¿à¤¦à¥à¤¯à¤¾à¤²à¤¯à¤®à¤¾ à¤¹à¤¾à¤² à¤•à¤•à¥à¤·à¤¾ à¥¯&ndash;à¥§à¥¦ à¤®à¤¾ à¤¸à¤¾à¤§à¤¾à¤°à¤£ à¤° à¤ªà¥à¤°à¤¾à¤µà¤¿à¤§à¤¿à¤• à¤—à¤°à¥€ à¤¦à¥à¤ˆ&nbsp;à¤¸à¤®à¥‚à¤¹à¤®à¤¾ à¤ªà¤ à¤¨à¤ªà¤¾à¤ à¤¨ à¤¸à¤žà¥à¤šà¤¾à¤²à¤¨ à¤—à¤°à¤¿à¤à¤•à¥‹ à¤› à¥¤</p>'),
(3, '../img/posts/5b974a52d9d00nepali model new new.jpg', 'Class 6-8', '<p>à¤¸à¥à¤µà¤°à¥‹à¤œà¤—à¤¾à¤°à¤®à¥‚à¤²à¤•, à¤œà¥€à¤µà¤¨à¥‹à¤ªà¤¯à¥‹à¤—à¥€ à¤° à¤µà¤¿à¤¶à¥à¤µ à¤µà¤œà¤¾à¤°à¤®à¤¾ à¤ªà¥à¤°à¤¤à¤¿à¤·à¥à¤ªà¤°à¥à¤§à¤¾ à¤—à¤°à¥à¤¨ à¤¸à¤•à¥à¤¨à¥‡ à¤œà¤¨à¤¶à¤•à¥à¤¤à¤¿ à¤‰à¤¤à¥à¤ªà¤¾à¤¦à¤¨ à¤—à¤°à¥à¤¨à¥‡ à¤‰à¤¦à¥à¤¦à¥‡à¤¶à¥à¤¯à¤•à¤¾ à¤¸à¤¾à¤¥ à¤¸à¥à¤¥à¤¾à¤ªà¤¨à¤¾ à¤­à¤à¤•à¥‹ à¤¯à¤¸ à¤µà¤¿à¤¦à¥à¤¯à¤¾à¤²à¤¯à¤®à¤¾ à¤¹à¤¾à¤² à¤•à¤•à¥à¤·à¤¾ à¥¯&ndash;à¥§à¥¦ à¤®à¤¾ à¤¸à¤¾à¤§à¤¾à¤°à¤£ à¤° à¤ªà¥à¤°à¤¾à¤µà¤¿à¤§à¤¿à¤• à¤—à¤°à¥€ à¤¦à¥à¤ˆ à¤¸à¤®à¥‚à¤¹ à¤° à¤•à¤•à¥à¤·à¤¾ à¥§à¥§&divide;à¥§à¥¨ à¤µà¤¿à¤œà¥à¤žà¤¾à¤¨, à¤•à¥ƒà¤·à¤¿ à¤µà¤¿à¤œà¥à¤žà¤¾à¤¨, à¤µà¥à¤¯à¤µà¤¸à¥à¤¥à¤¾à¤ªà¤¨, à¤®à¤¾à¤¨à¤µà¤¿à¤•à¥€ à¤° à¤¶à¤¿à¤•à¥à¤·à¤¾ à¤¸à¤®à¥‚à¤¹à¤®à¤¾ à¤ªà¤ à¤¨à¤ªà¤¾à¤ à¤¨ à¤¸à¤žà¥à¤šà¤¾à¤²à¤¨ à¤—à¤°à¤¿à¤à¤•à¥‹ à¤› à¤­à¤¨à¥‡ à¤¬à¤¦à¤²à¤¿à¤à¤¦à¥‹ à¤¸à¤®à¤¯ à¤•à¥à¤°à¤®à¤¸à¤à¤— à¤®à¤²à¥à¤Ÿà¤¿à¤ªà¥à¤°à¤µà¤¿à¤§à¤¿à¤¬à¤¾à¤Ÿ à¤¸à¤®à¥‡à¤¤ à¤ªà¤ à¤¨à¤ªà¤¾à¤ à¤¨ à¤ªà¥à¤°à¤•à¥à¤°à¤¿à¤¯à¤¾ à¤¸à¥à¤°à¥ à¤—à¤°à¤¿à¤à¤•à¥‹ à¤› à¥¤</p>'),
(4, '', 'Class 9-10', '<p>à¤¸à¥à¤µà¤°à¥‹à¤œà¤—à¤¾à¤°à¤®à¥‚à¤²à¤•, à¤œà¥€à¤µà¤¨à¥‹à¤ªà¤¯à¥‹à¤—à¥€ à¤° à¤µà¤¿à¤¶à¥à¤µ à¤µà¤œà¤¾à¤°à¤®à¤¾ à¤ªà¥à¤°à¤¤à¤¿à¤·à¥à¤ªà¤°à¥à¤§à¤¾ à¤—à¤°à¥à¤¨ à¤¸à¤•à¥à¤¨à¥‡ à¤œà¤¨à¤¶à¤•à¥à¤¤à¤¿ à¤‰à¤¤à¥à¤ªà¤¾à¤¦à¤¨ à¤—à¤°à¥à¤¨à¥‡ à¤‰à¤¦à¥à¤¦à¥‡à¤¶à¥à¤¯à¤•à¤¾ à¤¸à¤¾à¤¥ à¤¸à¥à¤¥à¤¾à¤ªà¤¨à¤¾ à¤­à¤à¤•à¥‹ à¤¯à¤¸ à¤µà¤¿à¤¦à¥à¤¯à¤¾à¤²à¤¯à¤®à¤¾ à¤¹à¤¾à¤² à¤•à¤•à¥à¤·à¤¾ à¥¯&ndash;à¥§à¥¦ à¤®à¤¾ à¤¸à¤¾à¤§à¤¾à¤°à¤£ à¤° à¤ªà¥à¤°à¤¾à¤µà¤¿à¤§à¤¿à¤• à¤—à¤°à¥€ à¤¦à¥à¤ˆ à¤¸à¤®à¥‚à¤¹ à¤° à¤•à¤•à¥à¤·à¤¾ à¥§à¥§&divide;à¥§à¥¨ à¤µà¤¿à¤œà¥à¤žà¤¾à¤¨, à¤•à¥ƒà¤·à¤¿ à¤µà¤¿à¤œà¥à¤žà¤¾à¤¨, à¤µà¥à¤¯à¤µà¤¸à¥à¤¥à¤¾à¤ªà¤¨, à¤®à¤¾à¤¨à¤µà¤¿à¤•à¥€ à¤° à¤¶à¤¿à¤•à¥à¤·à¤¾ à¤¸à¤®à¥‚à¤¹à¤®à¤¾ à¤ªà¤ à¤¨à¤ªà¤¾à¤ à¤¨ à¤¸à¤žà¥à¤šà¤¾à¤²à¤¨ à¤—à¤°à¤¿à¤à¤•à¥‹ à¤› à¤­à¤¨à¥‡ à¤¬à¤¦à¤²à¤¿à¤à¤¦à¥‹ à¤¸à¤®à¤¯ à¤•à¥à¤°à¤®à¤¸à¤à¤— à¤®à¤²à¥à¤Ÿà¤¿à¤ªà¥à¤°à¤µà¤¿à¤§à¤¿à¤¬à¤¾à¤Ÿ à¤¸à¤®à¥‡à¤¤ à¤ªà¤ à¤¨à¤ªà¤¾à¤ à¤¨ à¤ªà¥à¤°à¤•à¥à¤°à¤¿à¤¯à¤¾ à¤¸à¥à¤°à¥ à¤—à¤°à¤¿à¤à¤•à¥‹ à¤› à¥¤</p>'),
(6, '', '\nAgricultural Science', '<p>Agriculture program is designed for lower level human resources in the field of Agriculture services equipped with knowledge, skills and attitude necessary for this level of technicians so as to meet the demand of such technician in the country.</p>\n<p>This course is based on the job required to be performed by an agriculture sector in Nepal. This course intends to provide knowledge about following basic level Junior Technical Assistant. It especially provides the knowledge and skills focusing on Agriculture and farm management. To meet the man power needs of the various agricultural institutions in Nepal through the training for the Governmental and nongovernmental sector.</p>\n<p><strong>Aim:</strong>&nbsp;The aim of the program is to produce Junior Technical Assistant (JTA) in the field of Agriculture (plant science), to provide services to the people as a demand of the country.</p>\n<p><strong>Objectives: At the end of this course the trainee will able to:</strong></p>\n<p>1. Fulfill the demand of junior level manpower on the field of agriculture (plant science) of the country.</p>\n<p>2. Start their own business in the field in agriculture (plant science).</p>\n<p><strong>Salient features</strong><br /><strong>Duration:&nbsp;</strong>One Academic year in school and three months on the job training (1560 hrs.+ 3 months OJT)</p>\n<p><strong>Medium of Instruction:</strong>&nbsp;The medium of instruction will be in English and/or Nepali language.</p>\n<p><strong>Course Duration:</strong>&nbsp;This course will be completed within 15 months (1560 hours+ 480 hours OJT) including 3 months on-the-job training should also be completed for issuing successful completion of the course.</p>\n<p><strong>Pattern of Attendance:&nbsp;</strong>The students should have at list 90% attendance required to be eligible for internal assessments and final examinations.</p>\n<p><strong>Eligibility</strong><br />Individuals who meet the following criteria will be allowed to enter into this program:</p>\n<ul>\n<li>SLC pass</li>\n<li>Students should pass entrance examination administered by CTEVT</li>\n<li>They should submit the following documents at the time of application</li>\n<li>SLC pass certificate</li>\n<li>Character certificate</li>\n<li>Citizenship certificate (for the name, parents\' name, age, date of birth and address verification purpose only)</li>\n<li>Final selection will be made on the basis merit list.</li>\n<li>The quota for different category of students will be as per the enrollment policy of CTEVT</li>\n<li>Job prospectus</li>\n<li>The graduate will be eligible for the position equivalent to Non-gazetted 2nd class/level 4 (technical) as Junior Technical Assistant in the field of Agriculture or as prescribed by the Public Service Commission.</li>\n</ul>\n<p><strong>Affilation:</strong>&nbsp;CTEVT</p>\n<p><strong>&nbsp;Degree:</strong>&nbsp;Agriculture</p>\n<p><strong>&nbsp;Duration:</strong>&nbsp;1 year(s)3 month(s)</p>\n<p><strong>Careers</strong><br />Crop Farmers<br />Dairy Farmers<br />Pig and Poultry Farmers</p>'),
(10, '', '+2 Management', ''),
(11, '', '+2 Science', ''),
(12, '', 'Agriculture', ''),
(13, '', 'Humanities', ''),
(14, '', 'Education', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
